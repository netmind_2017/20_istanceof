import Class.Animal;
import Class.Person;

public class start {

	public static void main(String[] args) {
		
		Person person1 = new Person("Andrea","Gelsomino",40);
		Animal animal1 = new Animal("Toby","Gelsomino",40);
		Object p  = animal1;
		
		System.out.println(person1 instanceof Person); //true
		System.out.println(p instanceof Animal); //true
		
	}
}
