package Class;

public class Person {
	
	public String name;
	public String surname;
	public Integer age;
	
	public Person(String name,String surname,Integer age) {
		this.name = name;
		this.surname = surname;
		this.age = age;
	}

}
